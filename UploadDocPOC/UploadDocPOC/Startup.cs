﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UploadDocPOC.Startup))]
namespace UploadDocPOC
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
